package ru.tsc.panteleev.tm.command.task;

import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowListByProjectIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-list-show-by-project-id";

    public static final String DESCRIPTION = "Show task list by project id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(projectId,taskId);
    }
    
}
